let Websocket = require('ws');
let http = require("http");

mockData = {
    identifier: 'get-charts',
    data: ['test1', 'test2', 'test3', 'test4']
};
mockData2 = {
    identifier: 'get-charts',
    data: ['test1', 'test2', 'keinTest', 'test4']
};

const server = http.createServer();

const wss = new Websocket.Server({ server });
wss.on('connection', function connection(ws) {
    ws.on('message', function incomingMessage(payload) {
        let parsedPayload = JSON.parse(payload);
        console.log(parsedPayload);
        if(parsedPayload.identifier == 'get-charts') {
            console.log('received get-charts');
            ws.send(returnChartData(mockData));
        }
        if(parsedPayload.identifier == 'add-chart') {
            console.log('received add-chart');
            console.log(parsedPayload.data);
            let dataToAdd = mockData2.data;
            dataToAdd.push(parsedPayload.data);
            //let addedMockdata = mockData2.data.push(parsedPayload.data);
            console.log(dataToAdd);
            mockData2.data = dataToAdd;
            ws.send(returnChartData(mockData2));
        }
    })

    setTimeout(() => ws.send(returnChartData(mockData2)), 10000);

    ws.send(JSON.stringify(['connected']));
});

server.listen(5002, () => console.log('server started'));

function returnChartData(input) {
    console.log('returned');
    return JSON.stringify(input);
}