import { Component, OnInit } from '@angular/core';
import { OtDataProviderService } from './ot-data-provider.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'websocket-frontend';
  newChart: string;
  mockData$: Observable<number[]>;

  constructor(private otDataProviderService: OtDataProviderService){

  }

  ngOnInit(): void {
    this.mockData$ = this.otDataProviderService.listenForCharts();
    this.otDataProviderService.listenForSomethingElse();
    this.otDataProviderService.requestData('get-charts');
  }

  sendValue(): void {
    console.log(this.newChart);
    this.otDataProviderService.addChart(this.newChart);
  }
}
