import { TestBed } from '@angular/core/testing';

import { OtDataProviderService } from './ot-data-provider.service';

describe('OtDataProviderService', () => {
  let service: OtDataProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OtDataProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
