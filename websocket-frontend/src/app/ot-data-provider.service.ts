import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OtDataProviderService {

  private socket$: WebSocketSubject<any>;
  public charts$: Subject<string[]>;

  constructor() {
    this.socket$ = new WebSocketSubject('ws://localhost:5002');
  }

  addChart(data: any): void {
    this.socket$.subscribe();
    this.socket$.next({
      identifier: 'add-chart',
      data: data
    });
    //this.socket$.complete();
  }

  requestData(event: string): void {
    this.socket$.subscribe();
    this.socket$.next({
      identifier: event
    });
    //this.socket$.complete();
  }

  listenForCharts(): Observable<any> {
    return new Observable( observer => {
      this.socket$.subscribe(payload => {
        if(payload.identifier == 'get-charts') {
          console.log('get-charts');
          observer.next(payload.data);
        }
      })
    })
  }

  listenForSomethingElse(): any {
    this.socket$.subscribe(payload => {
      if(payload.identifier == 'something-else') {
        console.log('something else');
      }
    })
  }
}
